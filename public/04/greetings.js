import defaultGreetings from '../common/greetings-list.js';

export default class Greetings {
  constructor(customGreetings = null) {
    this.greetings = customGreetings || defaultGreetings;
  }

  greetingGetter() {
    return this.greetings[Math.floor(Math.random() * this.greetings.length)];
  }

  static youTellMeWhatToSay(phrase = 'Hello') {
    return phrase;
  }
}

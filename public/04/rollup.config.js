import babel from 'rollup-plugin-babel';
import { uglify } from 'rollup-plugin-uglify';
import path from 'path';

export default [
  {
    input: path.join(__dirname, 'index.js'),
    output: {
      file: path.join(__dirname, 'index.legacy.js'),
      format: 'iife',
      name: 'greetings'
    },
    plugins: [
      babel({
        presets: [
          [
            '@babel/preset-env',
            {
              targets: {
                ie: '11'
              }
            }
          ]
        ]
      }),
      uglify()
    ]
  }
];

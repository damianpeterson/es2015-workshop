import Greetings from './greetings.js';

const greetingElement = document.getElementById('greeting');

const greetings = new Greetings();
greetingElement.textContent = greetings.greetingGetter();

// If you call a static method you don't have to new-up the class
// greetingElement.textContent = Greetings.youTellMeWhatToSay('Hola');

const defaultGreetings = [
  'Hello',
  'Hola',
  'Kia ora',
  'здравствуйте',
  'ສະບາຍດີ',
  'Bonjour',
  'Goede dag',
  'Guten tag',
  'السلام عليكم'
];

export function greetingGetter(customGreetings = null) {
  const greetings = customGreetings || defaultGreetings;
  return greetings[Math.floor(Math.random() * greetings.length)];
}

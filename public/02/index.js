import { greetingGetter } from './greetings.js';

const greetingElement = document.getElementById('greeting');
greetingElement.textContent = greetingGetter();

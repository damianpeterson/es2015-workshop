import greetingsList from '../common/greetings-list.js';

export class Greetings {
  constructor(customGreetings = null) {
    this.greetings = customGreetings || greetingsList;
  }

  greetingGetter() {
    return this.greetings[Math.floor(Math.random() * this.greetings.length)];
  }

  static youTellMeWhatToSay(phrase = 'Hello') {
    return phrase;
  }
}

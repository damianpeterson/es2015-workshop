/**
 * A simple hash implementation to turn a string into a 32 bit signed integer
 * Identical to Java's String.hashCode() function
 *
 * @returns {number}
 */
export function generateHashCode(string) {
  let hash = 0;
  let i;
  let chr;

  if (string.length === 0) {
    return hash;
  }

  for (i = 0; i < string.length; i++) {
    chr = string.charCodeAt(i);
    hash = ((hash << 5) - hash) + chr; // eslint-disable-line no-bitwise
    hash |= 0; // eslint-disable-line no-bitwise
  }

  return hash;
}

/**
 * Create an array of numbers from an array of deltas and a starting number
 * https://jdwilliams.atlassian.net/wiki/spaces/UT/pages/435650672/How+to+hash+and+minify+a+list+of+bad+passwords
 *
 * @param deltaNumberArray
 * @param startingNumber
 */
export function createNumbersFromDeltaArray(deltaNumberArray, startingNumber) {
  if (typeof deltaNumberArray !== 'object' || deltaNumberArray.length < 1 || typeof startingNumber !== 'number') {
    return [];
  }

  return deltaNumberArray.map((delta) => {
    startingNumber += delta;
    return startingNumber;
  });
}

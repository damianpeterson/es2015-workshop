export function hasUpperCase(string) {
  return (string.match(/[A-Z]/) !== null);
}

export function hasLowerCase(string) {
  return (string.match(/[a-z]/) !== null);
}

export function hasNumber(string) {
  return (string.match(/[0-9]/) !== null);
}

export function hasOther(string) {
  return (string.match(/[^0-9A-Za-z]/) !== null);
}

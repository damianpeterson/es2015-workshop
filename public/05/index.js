import PasswordToggler from './components/password-toggler.js';
import PasswordStrengthIndicator from './components/password-strength-indicator.js';

const passwordToggler = new PasswordToggler(true);
const passwordStrengthIndicator = new PasswordStrengthIndicator(true);

/*
let togglerWrapper = document.getElementById('my-password').parentNode;
let toggler = document.createElement('div');

togglerWrapper.classList.add('password-toggler-wrapper');
toggler.classList.add('password-toggler');
toggler.dataset.target = 'my-password';
toggler.textContent = 'show';
togglerWrapper.appendChild(toggler);

passwordToggler.activateToggle(toggler);
*/

/**
 * Attach a show/hide toggler to a password field to switch between password and plain text.
 * Calling with autoInit = true will look for all instances on the page containing a class of .js-password-toggler
 * and attach them to a password field.
 */
export default class PasswordToggler {
  constructor(
    autoInit = false,
    defaultSelector = 'js-password-toggler',
    isToggledClass = 'password-toggler-is-active',
    hasTogglerClass = 'input-has-password-toggler'
  ) {
    this.isToggledClass = isToggledClass;
    this.hasTogglerClass = hasTogglerClass;

    if (autoInit === true) {
      const togglers = document.getElementsByClassName(defaultSelector);

      [...togglers].forEach((toggler) => {
        this.activateToggle(toggler);
      });
    }
  }

  handleToggleClick(toggler, passwordInput) {
    if (toggler.classList.contains(this.isToggledClass)) {
      toggler.classList.remove(this.isToggledClass);
      toggler.textContent = 'show';
      passwordInput.type = 'password';
    } else {
      toggler.classList.add(this.isToggledClass);
      toggler.textContent = 'hide';
      passwordInput.type = 'text';
    }

    passwordInput.focus();
  }

  activateToggle(toggler) {
    let passwordInput = document.getElementById(toggler.dataset.target);

    if (passwordInput) {
      passwordInput.classList.add(this.hasTogglerClass);
      toggler.classList.remove('hide');
      toggler.addEventListener('click', () => {
        this.handleToggleClick(toggler, passwordInput);
      });
    }
  }
}

import {
  hasLowerCase,
  hasNumber,
  hasOther,
  hasUpperCase
} from '../helpers/string-validation.js';
import {
  createNumbersFromDeltaArray,
  generateHashCode
} from '../helpers/encryption.js';

/**
 * Attach an indicator of strength for a password field.
 * Use autoInit = true to have it attempt to attach all strength helpers on the current page.
 */
export default class PasswordStrengthIndicator {
  constructor(
    autoInit = false,
    passwordStrengthSelector = '.js-password-strength',
    passwordActiveClass = 'input-has-strength',
    strengthFlagInvalid = 'invalid',
    strengthFlagPoor = 'poor',
    strengthFlagOk = 'ok',
    strengthFlagGood = 'good',
    scoreOk = 4,
    scoreGood = 9,
    messageInvalid = 'Please make your password longer.',
    messageCommon = 'This is a common password. Easy to guess.',
    messagePoor = 'This is easy to guess.',
    messagePoorSuggestionDefault = 'Try making it longer.',
    messagePoorSuggestionNoNumbers = 'Try adding a number.',
    messagePoorSuggestionNoUppercase = 'Try adding an uppercase letter.',
    messageOk = 'Good password. Difficult to guess.',
    messageGood = 'Excellent password! Very hard to guess.'
  ) {
    this.passwordStrengthSelector = passwordStrengthSelector;
    this.passwordActiveClass = passwordActiveClass;
    this.strengthFlagInvalid = strengthFlagInvalid;
    this.strengthFlagPoor = strengthFlagPoor;
    this.strengthFlagOk = strengthFlagOk;
    this.strengthFlagGood = strengthFlagGood;
    this.scoreOk = scoreOk;
    this.scoreGood = scoreGood;
    this.messageInvalid = messageInvalid;
    this.messageCommon = messageCommon;
    this.messagePoor = messagePoor;
    this.messagePoorSuggestionDefault = messagePoorSuggestionDefault;
    this.messagePoorSuggestionNoNumbers = messagePoorSuggestionNoNumbers;
    this.messagePoorSuggestionNoUppercase = messagePoorSuggestionNoUppercase;
    this.messageOk = messageOk;
    this.messageGood = messageGood;

    if (autoInit === true) {
      const strengthHelpers = document.querySelectorAll(this.passwordStrengthSelector);

      strengthHelpers.forEach((strengthHelper) => {
        this.attachHelper(strengthHelper);
      });
    }
  }

  /**
   * Attach a strength helper to its target password input and update it as the password changes
   *
   * @param strengthHelper
   */
  attachHelper(strengthHelper) {
    const passwordInput = document.getElementById(strengthHelper.dataset.target);

    if (!passwordInput || passwordInput.classList.contains(this.passwordActiveClass)) {
      return;
    }

    const { topPasswords, hashedPasswords } = PasswordStrengthIndicator.getCommonPasswords(strengthHelper);

    passwordInput.classList.add(this.passwordActiveClass);
    strengthHelper.classList.remove('hide');

    passwordInput.addEventListener('input', () => {
      this.updateStrength(passwordInput, strengthHelper, topPasswords, hashedPasswords);
    });
  }

  /**
   * Reads a comma-separated list of either plain-text common passwords, a hashed version or a delta of the hashed
   * version which is later rebuilt into the hashed version.
   *
   * @param strengthHelper
   * @returns {{topPasswords: any, hashedPasswords: any}}
   */
  static getCommonPasswords(strengthHelper) {
    const topPasswords = strengthHelper.dataset.topPasswords
      ? strengthHelper.dataset.topPasswords.split(',') : [];
    let hashedPasswords = strengthHelper.dataset.hashedPasswords
      ? strengthHelper.dataset.hashedPasswords.split(',').map(Number) : [];
    const deltaPasswords = strengthHelper.dataset.deltaPasswords
      ? strengthHelper.dataset.deltaPasswords.split(',').map(Number) : [];
    const firstPassword = parseInt(strengthHelper.dataset.firstPassword, 10) || null;

    if (topPasswords.length === 0
      && hashedPasswords.length === 0
      && deltaPasswords.length > 0
      && firstPassword !== null) {
      hashedPasswords = createNumbersFromDeltaArray(deltaPasswords, firstPassword);
    }

    return { topPasswords, hashedPasswords };
  }

  /**
   * Calculate the score of a password based on the length and variety of characters used.
   *
   * @param password
   * @param minLength
   * @returns {number}
   */
  static calculateScore(password, minLength = 8) {
    const numberOfCharacters = password.length;

    return Math.min(
      Math.min(10, numberOfCharacters),
      (
        Math.max(numberOfCharacters - minLength, 0)
        + (hasUpperCase(password) ? 2 : 0)
        + (hasLowerCase(password) ? 2 : 0)
        + (hasNumber(password) ? 2 : 0)
        + (hasOther(password) ? 3 : 0)
        + ((hasUpperCase(password) && hasLowerCase(password) && hasNumber(password) && hasOther(password))
          ? 4 : 0)
      )
    );
  }

  /**
   * Get the message of the strength helper
   *
   * @param dataset
   * @param score
   * @param password
   * @param minLength
   * @param isCommon
   */
  getStrengthMessage(dataset, score, password, minLength, isCommon) {
    let strengthMessage = dataset.messageGood || this.messageGood;

    if (password.length < minLength) {
      strengthMessage = dataset.messageInvalid || this.messageInvalid;
    } else if (isCommon) {
      strengthMessage = dataset.messageCommon || this.messageCommon;
    } else if (score <= this.scoreOk) {
      let messagePoor = dataset.messagePoor || this.messagePoor;
      let suggestion = dataset.messagePoorSuggestionDefault || this.messagePoorSuggestionDefault;
      if (password.length > minLength) {
        if (!hasNumber(password)) {
          suggestion = dataset.messagePoorSuggestionNoNumbers || this.messagePoorSuggestionNoNumbers;
        } else if (!hasUpperCase(password)) {
          suggestion = dataset.messagePoorSuggestionNoUppercase || this.messagePoorSuggestionNoUppercase;
        }
      }
      strengthMessage = `${messagePoor} ${suggestion}`;
    } else if (score <= this.scoreGood) {
      strengthMessage = dataset.messageOk || this.messageOk;
    }

    return strengthMessage;
  }

  /**
   * Get strength flag of the strength helper
   *
   * @param dataset
   * @param score
   * @param password
   * @param minLength
   * @param isCommon
   */
  getStrengthFlag(dataset, score, password, minLength, isCommon) {
    let strengthFlag = dataset.strengthFlagGood || this.strengthFlagGood;

    if (password.length < minLength) {
      strengthFlag = dataset.strengthFlagInvalid || this.strengthFlagInvalid;
    } else if (isCommon || score <= this.scoreOk) {
      strengthFlag = dataset.strengthFlagPoor || this.strengthFlagPoor;
    } else if (score <= this.scoreGood) {
      strengthFlag = dataset.strengthFlagOk || this.strengthFlagOk;
    }

    return strengthFlag;
  }

  /**
   * Calculate the strength of a password and update the strength helper accordingly
   *
   * @param passwordInput
   * @param strengthHelper
   * @param topPasswords
   * @param hashedPasswords
   */
  updateStrength(passwordInput, strengthHelper, topPasswords, hashedPasswords) {
    const password = passwordInput.value;
    const minLength = passwordInput.minLength || 8;
    let isCommon = false;

    // check against a list of common passwords once we hit the min length
    if (password.length >= minLength) {
      if (topPasswords.length > 0) {
        isCommon = (topPasswords.indexOf(password) > -1);
      } else if (hashedPasswords.length > 0) {
        isCommon = (hashedPasswords.indexOf(generateHashCode(password)) > -1);
      }
    }

    const score = PasswordStrengthIndicator.calculateScore(password, minLength);

    strengthHelper.dataset.score = score.toString();
    strengthHelper.dataset.strength = this.getStrengthFlag(
      strengthHelper.dataset, score, password, minLength, isCommon
    );
    strengthHelper.textContent = this.getStrengthMessage(strengthHelper.dataset, score, password, minLength, isCommon);
  }
}

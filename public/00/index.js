/* eslint-disable */
function varTest() {
  var x = 1;
  if (true) {
    var x = 2; // same variable!
    console.log(x); // 2
  }
  console.log(x); // 2
}

function letTest() {
  let x = 1;
  if (true) {
    let x = 2; // different variable
    console.log(x); // 2
  }
  console.log(x); // 1
}

varTest();
letTest();




















const people = [
  {
    first: 'Steve',
    last: 'Jones',
    sex: 'm',
    age: 21
  },
  {
    first: 'Jane',
    last: 'Hargreaves',
    sex: 'f',
    age: 54
  },
  {
    first: 'Mike',
    last: 'Linter',
    sex: 'm',
    age: 49
  },
  {
    first: 'Bad',
    last: undefined,
    sex: 'f',
    age: 1
  }
];

// people[0].first = 'New'; // allowed
// people = 'New'; // not allowed

function makeFullName(firstName = 'Bob', lastName = 'Smith') {
  return `${firstName} ${lastName}`;
}

people.forEach(({ first, last }) => {
  console.log(makeFullName(first, last));
});






















function makeFullNameTheOldWay() {
  var firstName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Bob';
  var lastName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'Smith';

  return firstName + ' ' + lastName;
}































for (let i = 0; i < people.length; i += 1) {
  console.log(makeFullName(people[i].first, people[i].last));
}

people.forEach((person) => {
  console.log(makeFullName(person.first, person.last));
});

people.forEach(({ first: f, last: l }) => {
  console.log(makeFullName(f, l));
});

people.map(({ first, last }) => {
  return makeFullName(first, last);
}).forEach(fullName => console.log(fullName));

import { assert } from 'chai';
import {
  generateHashCode,
  createNumbersFromDeltaArray
} from '../../05/helpers/encryption.js';

describe('Encryption helper', () => {
  describe('generateHashCode()', () => {
    it('correctly converts strings to hashed numbers', () => {
      let expectedConversions = new Map([
        ['', 0],
        ['a', 97],
        ['!@#$%^&*()_+', 705505834],
        ['😢', 1772933],
        ['😢😢😢😢😢😢😢😢', 1561164072],
        ['password', 1216985755],
        ['this is an incredibly long string for the purposes of ensuring we still get an integer', -1503235759],
        ['123', 48690],
        ['password', 1216985755],
        ['hyehyehye', -329168396]
      ]);

      expectedConversions.forEach((value, key) => {
        assert.equal(generateHashCode(key), value);
      });
    });
  });

  describe('createNumbersFromDeltaArray()', () => {
    it('handles an array with positive integers', () => {
      let testArray = [0, 2, 4, 0, 1000000];
      assert.deepEqual(createNumbersFromDeltaArray(testArray, 100), [100, 102, 106, 106, 1000106]);
    });

    it('handles an array with positive and negative integers', () => {
      let testArray = [0, 2, 4, 0, 1000000];
      assert.deepEqual(createNumbersFromDeltaArray(testArray, -100), [-100, -98, -94, -94, 999906]);
    });

    it('handles an array with only one item', () => {
      assert.deepEqual(createNumbersFromDeltaArray([4], 100), [104]);
    });

    it('fails gracefully when value supplied is invalid', () => {
      assert.deepEqual(createNumbersFromDeltaArray([], 100), []);
      assert.deepEqual(createNumbersFromDeltaArray([1, 2, 3], 'string'), []);
      assert.deepEqual(createNumbersFromDeltaArray([1, 2, 3], null), []);
      assert.deepEqual(createNumbersFromDeltaArray([1, 2, 3]), []);
    });
  });
});

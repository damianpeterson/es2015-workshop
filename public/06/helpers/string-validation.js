import { assert } from 'chai';
import {
  hasUpperCase,
  hasOther,
  hasNumber,
  hasLowerCase
} from '../../05/helpers/string-validation.js';

describe('String validation helper', () => {
  describe('hasLowerCase()', () => {
    it('should return true when a string has lower case characters', () => {
      assert.equal(hasLowerCase('a'), true);
      assert.equal(hasLowerCase('AAAAAAAaAAAAAA'), true);
    });
    it('should return false when a string doesn\'t have lower case characters', () => {
      assert.equal(hasLowerCase('A'), false);
      assert.equal(hasLowerCase('AAAAAAAäAAAAAA'), false);
    });
  });

  describe('hasUpperCase()', () => {
    it('should return true when a string has upper case characters', () => {
      assert.equal(hasUpperCase('A'), true);
      assert.equal(hasUpperCase('aaaaaaaAaaaaaa'), true);
    });
    it('should return false when a string doesn\'t have upper case characters', () => {
      assert.equal(hasUpperCase('a'), false);
      assert.equal(hasUpperCase('aaaaaaaÄaaaaaa'), false);
    });
  });

  describe('hasNumber()', () => {
    it('should return true when a string has a number', () => {
      assert.equal(hasNumber('1'), true);
      assert.equal(hasNumber('aaaaaaa1aaaaaa'), true);
    });
    it('should return false when a string doesn\'t have a number', () => {
      assert.equal(hasNumber('a'), false);
      assert.equal(hasNumber('aaaaaaaÄaaaaaa½'), false);
    });
  });

  describe('hasOther()', () => {
    it('should return true when a string has an other character', () => {
      const otherCharacters = '`~!@#$%^&*()_-+={}[]\\|:;"\'<>,.?/á£'.split();
      otherCharacters.forEach((character) => {
        assert.equal(hasOther(character), true);
      });
    });
    it('should return false when a string doesn\'t have an other character', () => {
      assert.equal(hasOther('a'), false);
      assert.equal(hasOther('A'), false);
      assert.equal(hasOther('1'), false);
      assert.equal(hasOther('aA1'), false);
    });
  });
});

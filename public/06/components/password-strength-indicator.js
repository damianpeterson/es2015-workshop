import { assert } from 'chai';
import 'jsdom-global/register';
import PasswordStrengthIndicator from '../../05/components/password-strength-indicator.js';

function createPasswordAndIndicator(
  passwordInputId = 'my-password',
  helperClassIdentifier = 'js-password-strength',
  clearHtml = true
) {
  if (clearHtml === true) {
    document.body.innerHTML = '';
  }

  let passwordInput = document.createElement('input');
  let strengthHelper = document.createElement('div');

  passwordInput.id = passwordInputId;
  passwordInput.type = 'password';
  passwordInput.minLength = 8;

  strengthHelper.classList.add('hide', helperClassIdentifier);
  strengthHelper.dataset.target = passwordInputId;

  document.body.appendChild(passwordInput);
  document.body.appendChild(strengthHelper);

  return { passwordInput, strengthHelper };
}

describe('Password strength indicator', () => {
  describe('Functional tests', () => {
    it('attaches to a element with a class of js-password-strength', () => {
      const { passwordInput, strengthHelper } = createPasswordAndIndicator();
      new PasswordStrengthIndicator(true);
      assert.equal(strengthHelper.classList.contains('hide'), false);
      assert.equal(passwordInput.classList.contains('input-has-strength'), true);
    });

    it('automatically attaches to a element with custom classes', () => {
      const { passwordInput, strengthHelper } = createPasswordAndIndicator(undefined, 'my-custom-class');
      new PasswordStrengthIndicator(true, '.my-custom-class', 'my-input');
      assert.equal(strengthHelper.classList.contains('hide'), false);
      assert.equal(passwordInput.classList.contains('my-input'), true);
    });

    it('manually attaches to a element with a custom class', () => {
      const { passwordInput, strengthHelper } = createPasswordAndIndicator(undefined, 'my-custom-class');
      const passwordStrengthHelper = new PasswordStrengthIndicator(undefined, undefined, 'my-input');
      passwordStrengthHelper.attachHelper(strengthHelper);
      assert.equal(strengthHelper.classList.contains('hide'), false);
      assert.equal(passwordInput.classList.contains('my-input'), true);
    });

    it('warns if less than 8 (or n) characters', () => {
      const { passwordInput, strengthHelper } = createPasswordAndIndicator();
      new PasswordStrengthIndicator(true);
      passwordInput.value = '1234567';
      passwordInput.dispatchEvent(new Event('input'));
      assert.equal(strengthHelper.textContent, 'Please make your password longer.');
      passwordInput.value = '12345678';
      passwordInput.dispatchEvent(new Event('input'));
      assert.notEqual(strengthHelper.textContent, 'Please make your password longer.');
      passwordInput.minLength = 5;
      passwordInput.value = '1234567';
      passwordInput.dispatchEvent(new Event('input'));
      assert.notEqual(strengthHelper.textContent, 'Please make your password longer.');
      passwordInput.value = '1234';
      passwordInput.dispatchEvent(new Event('input'));
      assert.equal(strengthHelper.textContent, 'Please make your password longer.');
      assert.equal(strengthHelper.dataset.strength, 'invalid');
    });

    it('warns of a common password from a plain text list', () => {
      const { passwordInput, strengthHelper } = createPasswordAndIndicator();
      strengthHelper.dataset.topPasswords = 'password,hyehyehye';
      new PasswordStrengthIndicator(true);
      passwordInput.value = 'password';
      passwordInput.dispatchEvent(new Event('input'));
      assert.equal(strengthHelper.textContent, 'This is a common password. Easy to guess.');
      assert.equal(strengthHelper.dataset.score, '2');
      assert.equal(strengthHelper.dataset.strength, 'poor');
      passwordInput.value = 'hyehyehye';
      passwordInput.dispatchEvent(new Event('input'));
      assert.equal(strengthHelper.textContent, 'This is a common password. Easy to guess.');
    });

    it('warns of a common password from a hashed list', () => {
      const { passwordInput, strengthHelper } = createPasswordAndIndicator();
      strengthHelper.dataset.hashedPasswords = '1216985755,-329168396';
      new PasswordStrengthIndicator(true);
      passwordInput.value = 'password';
      passwordInput.dispatchEvent(new Event('input'));
      assert.equal(strengthHelper.textContent, 'This is a common password. Easy to guess.');
      assert.equal(strengthHelper.dataset.score, '2');
      assert.equal(strengthHelper.dataset.strength, 'poor');
      passwordInput.value = 'hyehyehye';
      passwordInput.dispatchEvent(new Event('input'));
      assert.equal(strengthHelper.textContent, 'This is a common password. Easy to guess.');
    });

    it('warns of a common password from a hashed delta list', () => {
      const { passwordInput, strengthHelper } = createPasswordAndIndicator();
      strengthHelper.dataset.deltaPasswords = '0,1546154151';
      strengthHelper.dataset.firstPassword = '-329168396';
      new PasswordStrengthIndicator(true);
      passwordInput.value = 'password';
      passwordInput.dispatchEvent(new Event('input'));
      assert.equal(strengthHelper.textContent, 'This is a common password. Easy to guess.');
      assert.equal(strengthHelper.dataset.score, '2');
      assert.equal(strengthHelper.dataset.strength, 'poor');
      passwordInput.value = 'hyehyehye';
      passwordInput.dispatchEvent(new Event('input'));
      assert.equal(strengthHelper.textContent, 'This is a common password. Easy to guess.');
    });

    it('suggests making a simple password longer', () => {
      const { passwordInput, strengthHelper } = createPasswordAndIndicator();
      new PasswordStrengthIndicator(true);
      passwordInput.value = 'aaaaaaaa';
      passwordInput.dispatchEvent(new Event('input'));
      assert.equal(strengthHelper.textContent, 'This is easy to guess. Try making it longer.');
      assert.equal(strengthHelper.dataset.score, '2');
      assert.equal(strengthHelper.dataset.strength, 'poor');
    });

    it('suggests adding a number to a longish password', () => {
      const { passwordInput, strengthHelper } = createPasswordAndIndicator();
      new PasswordStrengthIndicator(true);
      passwordInput.value = 'aaaaaaaaaa';
      passwordInput.dispatchEvent(new Event('input'));
      assert.equal(strengthHelper.textContent, 'This is easy to guess. Try adding a number.');
      assert.equal(strengthHelper.dataset.score, '4');
      assert.equal(strengthHelper.dataset.strength, 'poor');
    });

    it('suggests adding an uppercase letter to a password that already has a number', () => {
      const { passwordInput, strengthHelper } = createPasswordAndIndicator();
      new PasswordStrengthIndicator(true);
      passwordInput.value = '1111111111';
      passwordInput.dispatchEvent(new Event('input'));
      assert.equal(strengthHelper.textContent, 'This is easy to guess. Try adding an uppercase letter.');
      assert.equal(strengthHelper.dataset.score, '4');
      assert.equal(strengthHelper.dataset.strength, 'poor');
    });

    it('suggests a short password with variety is good', () => {
      const { passwordInput, strengthHelper } = createPasswordAndIndicator();
      new PasswordStrengthIndicator(true);
      passwordInput.value = 'aA1!aaaa';
      passwordInput.dispatchEvent(new Event('input'));
      assert.equal(strengthHelper.textContent, 'Good password. Difficult to guess.');
      assert.equal(strengthHelper.dataset.score, '8');
      assert.equal(strengthHelper.dataset.strength, 'ok');
    });

    it('suggests a long password with variety is excellent', () => {
      const { passwordInput, strengthHelper } = createPasswordAndIndicator();
      new PasswordStrengthIndicator(true);
      passwordInput.value = 'aA1!aaaa and this is really long';
      passwordInput.dispatchEvent(new Event('input'));
      assert.equal(strengthHelper.textContent, 'Excellent password! Very hard to guess.');
      assert.equal(strengthHelper.dataset.score, '10');
      assert.equal(strengthHelper.dataset.strength, 'good');
    });

    it('doesn\'t attach when the target password input is missing', () => {
      document.body.innerHTML = '';
      let strengthHelper = document.createElement('div');
      strengthHelper.classList.add('hide', 'js-password-strength');
      strengthHelper.dataset.target = 'broken-target';
      document.body.appendChild(strengthHelper);
      new PasswordStrengthIndicator(true);
      assert.equal(strengthHelper.classList.contains('hide'), true);
    });
  });

  describe('calculateScore()', () => {
    it('correctly calculates scores', () => {
      assert.equal(PasswordStrengthIndicator.calculateScore(''), 0);
      assert.equal(PasswordStrengthIndicator.calculateScore('a'), 1);
      assert.equal(PasswordStrengthIndicator.calculateScore('aa'), 2);
      assert.equal(PasswordStrengthIndicator.calculateScore('Aa'), 2);
      assert.equal(PasswordStrengthIndicator.calculateScore('AAaa'), 4);
      assert.equal(PasswordStrengthIndicator.calculateScore('AAaa11'), 6);
      assert.equal(PasswordStrengthIndicator.calculateScore('aaaaaa'), 2);
      assert.equal(PasswordStrengthIndicator.calculateScore('AAAAAA'), 2);
      assert.equal(PasswordStrengthIndicator.calculateScore('111111'), 2);
      assert.equal(PasswordStrengthIndicator.calculateScore('------'), 3);
      assert.equal(PasswordStrengthIndicator.calculateScore('A!aa11'), 6);
      assert.equal(PasswordStrengthIndicator.calculateScore('aaaaaaaa'), 2);
      assert.equal(PasswordStrengthIndicator.calculateScore('AAAAaaaa'), 4);
      assert.equal(PasswordStrengthIndicator.calculateScore('AAAAaa11'), 6);
      assert.equal(PasswordStrengthIndicator.calculateScore('AA--aa11'), 8);
      assert.equal(PasswordStrengthIndicator.calculateScore('AA--aa11aaaaaaaaa'), 10);
      assert.equal(PasswordStrengthIndicator.calculateScore('this is a very long password'), 10);
    });
  });
});

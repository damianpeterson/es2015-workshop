import { assert } from 'chai';
import 'jsdom-global/register';
import PasswordToggler from '../../05/components/password-toggler.js';

function createPasswordAndToggler(
  passwordInputId = 'my-password',
  togglerClassIdentifier = 'js-password-toggler',
  clearHtml = true
) {
  if (clearHtml === true) {
    document.body.innerHTML = '';
  }

  let passwordInput = document.createElement('input');
  let toggler = document.createElement('div');

  passwordInput.id = passwordInputId;
  passwordInput.type = 'password';

  toggler.classList.add('hide', togglerClassIdentifier);
  toggler.textContent = 'show';
  toggler.dataset.target = passwordInputId;

  document.body.appendChild(passwordInput);
  document.body.appendChild(toggler);

  return { passwordInput, toggler };
}

describe('Password toggler', () => {
  it('automatically attaches a toggler to a password input', () => {
    const { passwordInput } = createPasswordAndToggler();
    new PasswordToggler(true);
    assert.equal(passwordInput.classList.contains('input-has-password-toggler'), true);
  });

  it('attaches to a custom-named toggler', () => {
    const { passwordInput } = createPasswordAndToggler('unique-id', 'my-toggler');
    new PasswordToggler(true, 'my-toggler');
    assert.equal(passwordInput.classList.contains('input-has-password-toggler'), true);
  });

  it('toggles from Show to Hide and back again when clicked and the input types change', () => {
    const { passwordInput, toggler } = createPasswordAndToggler();
    new PasswordToggler(true);
    assert.equal(toggler.textContent, 'show');
    assert.equal(passwordInput.type, 'password');
    toggler.click();
    assert.equal(toggler.textContent, 'hide');
    assert.equal(passwordInput.type, 'text');
    toggler.click();
    assert.equal(toggler.textContent, 'show');
    assert.equal(passwordInput.type, 'password');
  });

  it('toggles multiple togglers without affecting each other', () => {
    createPasswordAndToggler('my-password1', undefined, false);
    createPasswordAndToggler('my-password2', undefined, false);
    createPasswordAndToggler('my-password3', undefined, false);
    new PasswordToggler(true);
    let toggler1 = document.querySelector('[data-target="my-password1"]');
    let toggler2 = document.querySelector('[data-target="my-password2"]');
    let toggler3 = document.querySelector('[data-target="my-password3"]');
    let passwordInput1 = document.getElementById('my-password1');
    let passwordInput2 = document.getElementById('my-password2');
    let passwordInput3 = document.getElementById('my-password3');

    assert.equal(toggler1.textContent, 'show');
    assert.equal(passwordInput1.type, 'password');
    assert.equal(toggler2.textContent, 'show');
    assert.equal(passwordInput2.type, 'password');
    assert.equal(toggler3.textContent, 'show');
    assert.equal(passwordInput3.type, 'password');
    toggler1.click();
    assert.equal(toggler1.textContent, 'hide');
    assert.equal(passwordInput1.type, 'text');
    assert.equal(toggler2.textContent, 'show');
    assert.equal(passwordInput2.type, 'password');
    assert.equal(toggler3.textContent, 'show');
    assert.equal(passwordInput3.type, 'password');
    toggler2.click();
    assert.equal(toggler1.textContent, 'hide');
    assert.equal(passwordInput1.type, 'text');
    assert.equal(toggler2.textContent, 'hide');
    assert.equal(passwordInput2.type, 'text');
    assert.equal(toggler3.textContent, 'show');
    assert.equal(passwordInput3.type, 'password');
    toggler3.click();
    assert.equal(toggler1.textContent, 'hide');
    assert.equal(passwordInput1.type, 'text');
    assert.equal(toggler2.textContent, 'hide');
    assert.equal(passwordInput2.type, 'text');
    assert.equal(toggler3.textContent, 'hide');
    assert.equal(passwordInput3.type, 'text');
    toggler1.click();
    toggler2.click();
    toggler3.click();
    assert.equal(toggler1.textContent, 'show');
    assert.equal(passwordInput1.type, 'password');
    assert.equal(toggler2.textContent, 'show');
    assert.equal(passwordInput2.type, 'password');
    assert.equal(toggler3.textContent, 'show');
    assert.equal(passwordInput3.type, 'password');
  });
});

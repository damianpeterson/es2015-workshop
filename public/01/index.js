import { greetings } from './greetings.js';

const greetingElement = document.getElementById('greeting');
greetingElement.textContent = greetings[Math.floor(Math.random() * greetings.length)];

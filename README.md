In order to run this you'll need a modern version of nodejs and npm which you can get [here](https://nodejs.org/en/).

Then, from this directory, in your terminal run `npm install` which will bring in a bunch of node modules. From there
you can run the server from the same terminal with `node app.js` which will allow you to browse to http://localhost:3000

 - http://localhost:3000/01/ - ES2015 modules, import and export
 - http://localhost:3000/02/ - ES2015 more imports and exports but with functions
 - http://localhost:3000/03/ - ES2015 more imports and exports but with classes
 - http://localhost:3000/04/ - Making it work for older browsers
 - http://localhost:3000/05/ - A real world example
 - /public/06/ - Writing tests. Unit and functional along with coverage
